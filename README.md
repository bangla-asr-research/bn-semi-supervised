# Improving End-to-End Bangla Speech Recognition with Semi-supervised Training

This repository contains evaluation scripts used in our paper named “Improving End-to-End Bangla Speech Recognition with Semi-supervised Training” submitted to EMNLP 2020

## Requirements

- Python 3.6
- PyTorch 0.3.1

Optionally, GPU environment requires the following additional libraries:

- Cuda 9.0
- Cudnn 7

## Cloning the repository

To run the project, first clone this repository.
```sh
$ git clone https://bitbucket.org/bangla-asr-research/bn-semi-supervised.git --recursive
```
## Downloading the Corpus

The corpus used for the experiment can be found in the following link

```sh
https://drive.google.com/drive/folders/19-ytA2nEq1kLOhrkSOE-8TAGLOJNsrgL?usp=sharing
```
Download the files named asr_bengali.zip, bangla_corpus.zip and data.zip and place them in the root directory of the repository.

## Installation

### Step 1) setting of the environment for GPU support

To use cuda (and cudnn), make sure to set paths in your `.bashrc` or `.bash_profile` appropriately.
```
CUDAROOT=/usr/local/cuda
export PATH=$CUDAROOT/bin:$PATH
export CUDA_HOME=$CUDAROOT
export CUDA_PATH=$CUDAROOT
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/cuda/lib64:/usr/local/cuda/extras/CUPTI/lib64"
```

### Step 2) Installation

Install Python libraries and other required tools with [miniconda](https://conda.io/docs/glossary.html#miniconda-glossary)
```sh
$ cd bn-semi-supervised/espnet/tools
$ make PYTHON_VERSION=3.6 -f conda.mk
```

### Step 3) Unzip dataset
Move to the root directory of the repository ( the directory containing run.sh ) and unzip the dataset files.
```sh
$ cd ../..
$ unzip ./*.zip
```

### Step 4) Create relevant symbolic links
Move to the directory containing run.sh and make some symbolic links.

```sh
$ cd ../..
$ ln -s  ./espnet/egs/wsj/asr1/cmd.sh ./cmd.sh
$ ln -s  ./espnet/egs/wsj/asr1/steps ./steps
$ ln -s  ./espnet/egs/wsj/asr1/utils ./utils
$ ln -s  ./espnet/egs/wsj/asr1/conf  ./conf
$ ln -s  ./espnet/egs/wsj/asr1/local ./local
```

### Step 5) Check the directory structure
If all the steps mentioned above are done successfully, you should have a directory structure that looks like the following

```sh
├── asr_bengali
│   └── data
├── bangla_corpus
│   └── data
├── cmd.sh -> ./espnet/egs/wsj/asr1/cmd.sh
├── conf -> ./espnet/egs/wsj/asr1/conf
│ ├── lang_1char
│ ├── test
│ ├── train
│ ├── train_dev
│ ├── train_nodev
│ └── train_unpaired
├── edx.npy
├── edy.npy
├── espnet
│ ├── doc
│ ├── egs
│ ├── LICENSE
│ ├── README.md
│ ├── setup.cfg
│ ├── src
│ ├── test
│ └── tools
├── exp
├── LICENSE
├── local -> ./espnet/egs/wsj/asr1/local
├── path.sh
├── python
│ ├── asr_train_loop_th.py
│ ├── results.py
│ ├── retrain_loop_th.py
│ ├── unsupervised.py
│ └── unsupervised_recog_th.py
├── README.md
├── run.sh
├── sbatch.sh
├── shell
├── steps -> ./espnet/egs/wsj/asr1/steps
└── utils -> ./espnet/egs/wsj/asr1/utils
```


### Step 6) Run code

```sh
$ ./run.sh
```

## References
[1] Shinji Watanabe, Takaaki Hori, Shigeki Karita, Tomoki Hayashi, Jiro Nishitoba, Yuya Unno, Nelson Enrique Yalta Soplin, Jahn Heymann, Matthew Wiesner, Nanxin Chen, Adithya Renduchintala, and Tsubasa Ochiai, "ESPnet: End-to-End Speech Processing Toolkit," *Proc. Interspeech'18*, pp. 2207-2211 (2018)

[2] Shigeki Karita, Shinji Watanabe, Tomoharu Iwata, Atsunori Ogawa, and Marc Delcroix. 2018. Semi-supervised end-to-end speech recognition. In Interspeech, pages 2–6.
